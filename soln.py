import tkinter as tk
import tkinter.font as tkFont



#prob = [[5, 2, 3, 0, 0, 9, 8, 7, 6], [4, 1, 7, 0, 2, 0, 3, 9, 5], [6, 0, 9, 5, 7, 3, 1, 2, 4], [8, 7, 4, 3, 6, 1, 2, 0, 9], [3, 6, 0, 9, 5, 2, 4, 8, 0], [2, 0, 0, 0, 4, 7, 6, 1, 3], [9, 5, 6, 0, 8, 0, 7, 0, 0], [7, 3, 0, 1, 9, 6, 5, 0, 0], [1, 4, 0, 7, 3, 5, 9, 6, 2]]


def grid_layout_soln(mW, grid_dim, prob, array):
    entries = []
    for row in range(grid_dim):
        for col in range(grid_dim):
            pad_y = (0, 0)
            pad_x = (0, 0)
            if (row+1) % 3 == 0 and (row+1) < grid_dim: # skip for last row
                pad_y = (0, 10)
            if (col+1) % 3 == 0 and (col+1) < grid_dim: # skip for last column
                pad_x = (0, 10)

            if prob[row][col] == 0:
                    fontStyle = tkFont.Font(family="Helvetica", size=30)
                    label = tk.Label(mW, text= str(array[row][col]), font=fontStyle, borderwidth=2, relief="solid", bg='red')
                    label.grid(row=row, column=col, ipadx=30, ipady=20, padx=pad_x, pady=pad_y)
            else:
                    fontStyle = tkFont.Font(family="Helvetica", size=30)
                    label = tk.Label(mW, text= str(array[row][col]), font=fontStyle, borderwidth=2, relief="solid", bg='yellow')
                    label.grid(row=row, column=col, ipadx=30, ipady=20, padx=pad_x, pady=pad_y)
    return entries


# --- main ---

def GUIforSudokuSoln(prob, array):
	mW = tk.Tk()
	mW.title('SOlution')
	width = int(mW.winfo_screenwidth())
	height =  int(mW.winfo_screenheight())
	mW.geometry("{}x{}".format(width,height))
	entries = grid_layout_soln(mW, 9, prob, array)  # send result 
	mW.mainloop()



