import tkinter as tk
import tkinter.font as tkFont
from soln import *
from constraint import *
import random

rows = 'abcdefghi'
cols = '123456789'
digits = range(1, 10)
vars = [row + col for row in rows for col in cols]
rowgroups =     [
                        [row + col for col in cols]
                        for row in rows
                ]
colgroups =     [
                        [row + col for row in rows]
                        for col in cols
                ]
squaregroups = [
                [rows[3 * rowgroup + k] + cols[3 * colgroup + j]
                        for j in range(3)
                        for k in range(3)]
                        for colgroup in range(3)
                        for rowgroup in range(3)
                ]

def solve(hints):
        problem = Problem()
        for var, hint in zip(vars, hints):
                problem.addVariables([var], [hint] if hint in digits else digits)
        for vargroups in [rowgroups, colgroups, squaregroups]:
                for vargroup in vargroups:
                        problem.addConstraint(AllDifferentConstraint(), vargroup)
        return problem.getSolutions()
def pretty(var_to_value):
        board = [[0 for i in range(9)] for j in range(9)]
        for rownum, row in enumerate('abcdefghi'):
                for colnum, col in enumerate('123456789'):
                        board[rownum][colnum] = int(var_to_value[0][row+col])
        return board

#prob = [[5, 2, 3, 0, 0, 9, 8, 7, 6], [4, 1, 7, 0, 2, 0, 3, 9, 5], [6, 0, 9, 5, 7, 3, 1, 2, 4], [8, 7, 4, 3, 6, 1, 2, 0, 9], [3, 6, 0, 9, 5, 2, 4, 8, 0], [2, 0, 0, 0, 4, 7, 6, 1, 3], [9, 5, 6, 0, 8, 0, 7, 0, 0], [7, 3, 0, 1, 9, 6, 5, 0, 0], [1, 4, 0, 7, 3, 5, 9, 6, 2]]

def grid_layout(mW, grid_dim, array):
    entries = []
    for row in range(grid_dim):
        for col in range(grid_dim):
            pad_y = (0, 0)
            pad_x = (0, 0)
            if (row+1) % 3 == 0 and (row+1) < grid_dim: # skip for last row
                pad_y = (0, 10)
            if (col+1) % 3 == 0 and (col+1) < grid_dim: # skip for last column
                pad_x = (0, 10)

            if array[row][col] == 0:
                    entry = tk.Entry(mW, width=1, highlightthickness=1, highlightbackground='#000000', font = "Helvetica 30", justify="center")
                    entry.grid(row=row, column=col, ipadx=30, ipady=20, padx=pad_x, pady=pad_y)
                    entries.append(entry)
            else:
                    fontStyle = tkFont.Font(family="Helvetica", size=30)
                    label = tk.Label(mW, text= str(array[row][col]), font=fontStyle, borderwidth=2, relief="solid", bg='yellow')
                    label.grid(row=row, column=col, ipadx=30, ipady=20, padx=pad_x, pady=pad_y)
    b = tk.Button(mW,text = "Get Solution", command=lambda: solution(array, mW))
    b.grid(row=5, column=12, ipadx=20, ipady=20, padx=0, pady=0)
    return entries



# --- main ---
def GUIforSudoku(prob):
	mW = tk.Tk()
	mW.title('Sudoku')
	width = int(mW.winfo_screenwidth())
	height =  int(mW.winfo_screenheight())
	mW.geometry("{}x{}".format(width,height))
	entries = grid_layout(mW, 9, prob)  # send result 
	mW.mainloop()


def solution(array, mW):
        mW.destroy()
        soln = pretty(solve(hints))
        GUIforSudokuSoln(array, soln)

hints = [5, 2, 3, 0, 0, 9, 8, 7, 6, 4, 1, 7,  0, 2, 0,  3, 9, 5, 6, 0, 9,  5, 7, 3,  1, 2, 4, 8, 7, 4,  3, 6, 1,  2, 0, 9, 3, 6, 0,  9, 5, 2,  4, 8, 0, 2, 0, 0,  0, 4, 7,  6, 1, 3, 9, 5, 6,  0, 8, 0,  7, 0, 0, 7, 3, 0,  1, 9, 6,  5, 0, 0, 1, 4, 0,  7, 3, 5,  9, 6, 2]
'''file = open("EasyProbSet.csv","r").read()
arrays = file.split("\n")
x = random.randint(1, 2)
pull_list = arrays[0]

hints = pull_list[1:-1].split(", ")
'''

arrayHints = [[0 for i in range(9)] for j in range(9)]

x = 0
for i in range(9):
        for j in range(9):
                arrayHints[i][j] = int(hints[x])
                x += 1

GUIforSudoku(arrayHints)
