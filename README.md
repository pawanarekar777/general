# Sudoku Solver

`By pawan and dnyaneshwar`

## Problem Description:

- This is a Constraint Satisfation Problem in AI.
- Typically sudoku is a 9x9 grid which have some numbers printed on it initially.
- There are many voids in sudoku grid Which we have to Occupy using numbers between 1 to 9.
- The Number of voids are directly Proportional to Hardness of Sudoku.

## Constraints:

- A number should not occur twice in a row.
- A number should not occur twice in a column.
- There are 9 subgrids of dimension 3x3.
- A number should not occur twice in a subgrid.

## Files:

### `sudoku.py`:

- Show Initial Sudoku Grid which have to be solved.
- Solve the sudoku.

### `soln.py`:

- Show the Answer of Solved Sudoku Grid Using a GUI.

## Function Description:

### `solve()`:

- Add constraints to the sudoku.
- Get a solution.

### `pretty()`:

- Get output in an array.
### `grid_layout`:
- Create a GUI for Sudoku.
